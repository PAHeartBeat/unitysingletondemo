﻿/*
 * File: GUIManager.cs
 * ===================
 * 
 * Version				: 1.0
 * Author				: Ankur Ranpariya 
 * E-Mail				: ankur30884@gmail.com
 * Copyright			: Not Available
 * Company				: Not Available
 * Script Location		: Plugins/Other
 * 
 * 
 * Created By			: Ankur Ranpariya
 * Created Date			: 2014.11.28
 * 
 * Last Modified by		: Ankur Ranpariya on 2014.11.28 (ankur30884@gmail.com / @PA_HeartBeat)
 * Last Modified		: 2014.11.28
 * 
 * Contributors 		: Sharatbabu Achary
 * Curtosey By			: 
 * 
 * Purpose
 * ====================================================================================================================
 * Script is writen for elobrate uses of diffrent type of Singleton Monobehaviour in unity 3d
 * 
 * 
 * 
 * ====================================================================================================================
 * LICENCE / WARRENTY
 * ==================
 * Copyright (c) 2014 <<Developer Name / Company name>> 
 * Please direct any bugs/comments/suggestions to <<support email id>>
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 * 
 * 
 * Change Log
 * ====================================================================================================================
 * v1.0
 * ====
 * 1. Initial version by "Ranpariya Ankur"
 * 
 * ====================================================================================================================
*/


using UnityEngine;

/// <summary>
/// <para filename="GUIManger.cs" /> 
/// <para classname="GUIManager" > 
/// <para version="1.0.0.0" />	 
/// <para author="Ranpariya Ankur" />
/// <para support="" />
/// <para>
/// Description: 
/// </para>
/// </summary>
public class GUIManager : MonoBehaviour {

	// Use this for initialization
	//void Start() {
	//}
	// Update is called once per frame
	//void Update() {
	//}


	/// <summary>
	/// Raises the GU event.
	/// uses for demo print buttons for demo perpose
	/// </summary>
	void OnGUI() {
		if(GUI.Button(new Rect(10, 10, 200, 30), "Auto")) {
			AutoExample.Me.PrintMessage();
		}

		if(GUI.Button(new Rect(10, 50, 200, 30), "Regular")) {
			RegularExample.Me.PrintMessage();
		}

		if(GUI.Button(new Rect(10, 90, 200, 30), "With Prefab")) {
			WithPrefab.Me.PrintMessage();
		}

		if(GUI.Button(new Rect(10, 130, 200, 30), "Without Prefab")) {
			WithoutPrefab.Me.PrintMessage();
		}

		if(GUI.Button(new Rect(10, 170, 200, 30), "Prefab Without Script")) {
			PrefabWithoutScript.Me.PrintMessage();
		}

		if(GUI.Button(new Rect(10, 210, 200, 30), "Prefab No Attribute")) {
			PrefabNoAttribute.Me.PrintMessage();
		}

	}
}
