﻿/*
 * File: AutoExample.cs
 * ====================
 * 
 * Version				: 1.0
 * Author				: Ankur Ranpariya 
 * E-Mail				: ankur30884@gmail.com
 * Copyright			: Not Available
 * Company				: Not Available
 * Script Location		: Plugins/Other
 * 
 * 
 * Created By			: Ankur Ranpariya
 * Created Date			: 2014.11.28
 * 
 * Last Modified by		: Ankur Ranpariya on 2014.11.28 (ankur30884@gmail.com / @PA_HeartBeat)
 * Last Modified		: 2014.11.28
 * 
 * Contributors 		: Sharatbabu Achary
 * Curtosey By			: 
 * 
 * Purpose
 * ====================================================================================================================
 * Script is writen for elobrate uses of "SingletonAuto" Script in Unity 3D and how it will behave with diffrent kind
 * of senrio like 
 * 1. What if script not attahced in any objects
 * 2. Wgat if script will have morethen one instances in screen
 * 3. what if script attached on only one object
 * 
 * ====================================================================================================================
 * LICENCE / WARRENTY
 * ==================
 * Copyright (c) 2014 <<Developer Name / Company name>> 
 * Please direct any bugs/comments/suggestions to <<support email id>>
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 * 
 * 
 * Change Log
 * ====================================================================================================================
 * v1.0
 * ====
 * 1. Initial version by "Ranpariya Ankur"
 * 
 * ====================================================================================================================
*/


using UnityEngine;
/// <summary>
/// <para filename="AutoExample.cs" /> 
/// <para classname="AutoExample" > 
/// <para version="1.0.0.0" />	 
/// <para author="Ranpariya Ankur" />
/// <para support="" />
/// <para>
/// Description: 
/// </para>
/// </summary>
public class AutoExample : SingletonAuto<AutoExample> {

	/// <summary>
	/// Prints demo messages for tessting Singleton settings
	/// </summary>
	public void PrintMessage() {
		Debug.Log("Log from AutoExample Script");
	}
}